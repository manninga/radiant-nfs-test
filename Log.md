# Activity log

## 2022/10/27

The new test cluster is online with minimal deployments.

We need to:

* Request a DNS A record pointing `nfs-test.ncsa.illinois.edu` to the floating IP

* Obtain the associated private IP to configure MetalLB

* Bootstrap applications by installing ArgoCD via Helm as shown below

  ```bash
  helm repo add decentci https://decentci.gitlab.io/charts/
  helm repo update
  ```

  Customize parameters using `/tmp/argocd.values.yaml` file:

  ```yaml
  ingress:
    hostname: "nfs-test.ncsa.illinois.edu"
  argo-cd:
    configs:
      secret:
        createSecret: true

  ```

  ```bash
  helm install -n argo-cd --create-namespace argocd decentci/argo-cd -f /tmp/argocd.values.yaml 

  ```

* Manually create the root app in ArgoCD:

  ```yaml
  apiVersion: argoproj.io/v1alpha1
  kind: Application
  metadata:
    name: 'decentci'
  spec:
    project: default
    source:
      repoURL: 'https://git.ncsa.illinois.edu/manninga/radiant-nfs-test.git'
      path: apps/decentci
      targetRevision: main
    destination:
      server: 'https://kubernetes.default.svc'
      namespace: argo-cd
  ```

* Sync the Sealed Secrets app and, seal the Nextcloud secrets, and store them in the Nextcloud sidecar app.

* Sync the rest of the apps including Nextcloud.

Create `secrets.yaml` file with Nextcloud secrets and seal with helper script:

```bash
python seal_bulk_secrets.py --file ./secrets/secrets.yaml
```

## 2022/10/28

Deployed Nextcloud.

Switched to letsencrypt-production certificate issuer for Cert Manager to start using real TLS certs.

Requested DNS records `nfs-test.ncsa.illinois.edu` and `cloud.nfs-test.ncsa.illinois.edu`.

Created a new OIDC client `nfs-test-nextcloud` in the Antares realm of the existing Keycloak server https://keycloak.antares.ncsa.illinois.edu operated for another project. Client secret is stored in the Nextcloud sealed secrets in the sidecar app.

Added SSH private key as a sealed secret in the root DecentCI app.

Installed Social Login app and granted others admin rights to server when logging in via Keycloak.

Installed Group Folders app and created an admin group share called `bucket`

Enabled Nextcloud backups to Taiga.
