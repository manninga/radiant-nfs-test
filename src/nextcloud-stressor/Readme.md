# Nextcloud Stressor

This Python package automates stress testing of a Nextcloud service, by reading and writing files.

## Configuration

Copy `env.example.txt` to `.env` and set your Nextcloud "app password" credentials obtained via [the Security settings page](https://cloud.nfs-test.ncsa.illinois.edu/settings/user/security). The `RANDOM_BIAS` is an integer between 0 and 100 that sets the probability that a random action will occur when randomness is enabled for various functions.

## Development

Run `docker compose up` to run the stressor or `docker compose up --build` to build the container first.
