import os
from nextcloud import NextCloud
from random import choice
# Configure logging
import logging
logging.basicConfig(
    format='%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')
log = logging.getLogger("app")
try:
    log.setLevel(os.environ['LOG_LEVEL'].upper())
except:
    log.setLevel('WARNING')

NEXTCLOUD_URL = os.environ.get('NEXTCLOUD_URL')
NEXTCLOUD_USERNAME = os.environ.get('NEXTCLOUD_USERNAME')
NEXTCLOUD_PASSWORD = os.environ.get('NEXTCLOUD_PASSWORD')
RANDOM_BIAS = int(os.environ.get('RANDOM_BIAS', "50"))


class NextcloudClient():
    def __init__(self, url, username, password, random_bias=50):
        self.base_url = url
        self.username = username
        self.password = password
        self.random_bias = random_bias
        self.nxc = NextCloud(NEXTCLOUD_URL, user=NEXTCLOUD_USERNAME,
                             password=NEXTCLOUD_PASSWORD)

    def random_boolean(self):
        return choice(range(0, 100)) < self.random_bias

    def test_random_boolean(self):
        idx = yin = yang = 0
        while idx < 1e7:
            idx += 1
            if self.random_boolean():
                yin += 1
            else:
                yang += 1
        log.info(f'''yin: {yin}, yang: {yang}, ratio (target is {self.random_bias}%): {round(100.0*yin/idx, 2)}''')

    def download_file(self, filepath):
        file = self.nxc.get_file(filepath)
        file.download(path=None, target=None, overwrite=None)

    def list_all_files(self, root_path):
        def _list_rec(d, indent=""):
            # list files recursively
            log.info("%s%s%s" % (indent, d.basename(), '/' if d.isdir() else ''))
            if d.isdir():
                for i in d.list():
                    _list_rec(i, indent=indent+"  ")
        _list_rec(self.nxc.get_folder(root_path))

    def upload_file(self, local_filepath, target_dirpath, target_filename):
        assert os.path.isfile(local_filepath)
        dir_obj = self.nxc.get_folder(target_dirpath)
        assert dir_obj.isdir()
        target_filepath = os.path.join(target_dirpath, target_filename)
        file_obj = self.nxc.get_file(target_filepath)
        if file_obj and file_obj.isfile():
            log.debug(f'File already exists: {target_filepath}')
        else:
            # log.debug(f'''Uploading file: {local_filepath}''')
            dir_obj.upload_file(local_filepath, target_filename)

    def delete_folder(self, target_dirpath, random=False):
        def random_delete(dir_obj):
            if dir_obj.isdir():
                if self.random_boolean():
                    # log.debug(f'''Deleting folder {dir_obj.get_relative_path()}''')
                    dir_obj.delete(subpath='', recursive=True)
                for item in dir_obj.list():
                    random_delete(item)
        if random:
            random_delete(self.nxc.get_folder(target_dirpath))
        else:
            dir_obj = self.nxc.get_folder(target_dirpath)
            if dir_obj.isdir():
                # log.debug(f'Deleting folder: {target_dirpath}')
                dir_obj.delete(subpath='', recursive=True)
            else:
                log.debug(f'''Folder does not exist: {target_dirpath}''')

    def upload_folder(self, local_dirpath, target_dirpath, target_dirname, random=False):
        assert os.path.isdir(local_dirpath)
        target_root = os.path.join(target_dirpath, target_dirname)
        # log.debug(f'''local_dirpath: {local_dirpath}''')
        # log.debug(f'''target_root: {target_root}''')
        self.nxc.create_folder(target_root)
        for local_root, dirs, files in os.walk(local_dirpath):
            current_target_dir = os.path.join(target_root, local_root)
            dir_obj = self.nxc.get_folder(current_target_dir)
            if dir_obj.isdir():
                log.debug(f'Directory already exists: {current_target_dir}')
            else:
                log.debug(f'''Creating folder: {current_target_dir}''')
                self.nxc.create_folder(current_target_dir)
                dir_obj = self.nxc.get_folder(current_target_dir)
            for filename in files:
                local_filepath = os.path.join(local_root, filename)
                if not random or self.random_boolean():
                    self.upload_file(
                        local_filepath, current_target_dir, filename)


def main():
    client = NextcloudClient(
        url=NEXTCLOUD_URL, username=NEXTCLOUD_USERNAME, password=NEXTCLOUD_PASSWORD, random_bias=RANDOM_BIAS)

    root_dir = '/bucket/stressor'
    try:
        response = client.nxc.create_folder(root_dir, already_exists=True)
        log.debug(response)
        log.debug(response.get_error_message())
    except Exception as e:
        log.error(e)
    log.debug('Files before upload:')
    client.list_all_files(root_dir)
    log.debug('Upload files...')
    client.upload_folder(os.path.join(os.path.dirname(__file__), 'files'), root_dir, 'test_files', random=True)
    log.debug('Files after upload:')
    client.list_all_files(root_dir)
    log.debug('Delete files...')
    client.delete_folder(os.path.join(root_dir, 'test_files'), random=True)
    log.debug('Files after deletion:')
    client.list_all_files(root_dir)
    return


if __name__ == "__main__":
    main()
