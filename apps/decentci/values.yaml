##
## DecentCI Infrastructure
##
infrastructure:
  argocd:
    enabled: true
    values:
      ingress:
        hostname: "nfs-test.ncsa.illinois.edu"
      argo-cd:
        server:
          config:
            url: "https://nfs-test.ncsa.illinois.edu/argo-cd"
            repositories: |
              - type: helm
                name: stable
                url: https://charts.helm.sh/stable
              - type: helm
                name: argo-cd
                url: https://argoproj.github.io/argo-helm
              - type: helm
                name: traefik
                url: https://helm.traefik.io/traefik
              - type: helm
                name: metallb
                url: https://metallb.github.io/metallb
              - type: helm
                name: longhorn
                url: https://charts.longhorn.io
              - type: helm
                name: nfs-subdir-external-provisioner
                url: https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner
              - type: git
                url: https://gitlab.com/decentci/charts.git
              - type: git
                url: https://git.ncsa.illinois.edu/manninga/radiant-nfs-test.git
        configs:
          secret:
            createSecret: true
  sealedsecrets:
    enabled: true
  certmanager:
    enabled: true
    values:
      cert-manager:
        ingressShim:
          defaultIssuerName: letsencrypt-production
      clusterIssuer:
        email: "manninga@illinois.edu"
  traefik:
    enabled: true
    values:
      service:
        externalIP:
        - 141.142.216.96
        spec:
          loadBalancerIP: "192.168.0.81"
      additionalArguments:
      - --providers.kubernetesingress.ingressendpoint.ip=141.142.216.96
  metallb:
    enabled: true
    values:
      configInline:
        address-pools:
        - name: default
          protocol: layer2
          addresses:
          - 192.168.0.81/32
  longhorn:
    enabled: true
    values:
      defaultSettings:
        backupTarget: ""
  nfsprovisioner:
    enabled: true
    values:
      storageClass:
        name: nfs-taiga
      nfs:
        server: "set-nfstest.ncsa.illinois.edu"
        path: "/taiga/ncsa/radiant/bbdb/nfs-test/pv/nfs-taiga"
##
## DecentCI Services
##
services:
  nextcloud:
    enabled: true
    sidecar:
      enabled: true
      repoURL: https://git.ncsa.illinois.edu/manninga/radiant-nfs-test.git
      path: "apps/decentci/sidecars/nextcloud-sidecar"
      targetRevision: "main"
    values:
      nextcloud:
        #########################################################################
        ## When upgrading Nextcloud, use delayed startup probe and single replica
        ##
        startupProbe:
          enabled: true
          initialDelaySeconds: 10
          periodSeconds: 30
          timeoutSeconds: 30
          failureThreshold: 40
          successThreshold: 1
        # replicaCount: 1
        ##
        #########################################################################
        replicaCount: 3
        ingress:
          tls:
          - secretName: "cloud-nfs-test-ncsa-illinois-edu-tls"
            hosts:
            - "cloud.nfs-test.ncsa.illinois.edu"
        nextcloud:
          host: cloud.nfs-test.ncsa.illinois.edu
          configs:
            extra.config.php: |-
              <?php
              $CONFIG = array (
                'trusted_proxies' => array('traefik'),
                'mail_smtphost' => 'smtp.ncsa.uiuc.edu',
                'mail_from_address' => 'devnull',
                'mail_domain' => 'ncsa.illinois.edu',
                'overwriteprotocol' => 'https',
                'overwrite.cli.url' => 'https://cloud.nfs-test.ncsa.illinois.edu'
              );
        cronjob:
          enabled: false
      backups:
        enabled: false
        volume:
          nfs:
            basePath: "/taiga/ncsa/radiant/bbdb/nfs-test/backups/nextcloud"
            server: "taiga-nfs.ncsa.illinois.edu"
        data:
          enabled: false

services2:
  nextcloud:
    enabled: true
    namespace: nextcloud2
    app_name: nextcloud2
    sidecar:
      enabled: true
      namespace: nextcloud2
      app_name: nextcloud2-sidecar
      repoURL: https://git.ncsa.illinois.edu/manninga/radiant-nfs-test.git
      path: "apps/decentci/sidecars/nextcloud-sidecar"
      targetRevision: "main"
    values:
      nextcloud:
        #########################################################################
        ## When upgrading Nextcloud, use delayed startup probe and single replica
        ##
        startupProbe:
          enabled: true
          initialDelaySeconds: 10
          periodSeconds: 30
          timeoutSeconds: 30
          failureThreshold: 40
          successThreshold: 1
        # replicaCount: 1
        ##
        #########################################################################
        replicaCount: 3
        persistence:
          storageClass: "nfs-taiga"
        ingress:
          tls:
          - secretName: "cloud2-nfs-test-ncsa-illinois-edu-tls"
            hosts:
            - "cloud2.nfs-test.ncsa.illinois.edu"
        nextcloud:
          host: cloud2.nfs-test.ncsa.illinois.edu
          configs:
            extra.config.php: |-
              <?php
              $CONFIG = array (
                'trusted_proxies' => array('traefik'),
                'mail_smtphost' => 'smtp.ncsa.uiuc.edu',
                'mail_from_address' => 'devnull',
                'mail_domain' => 'ncsa.illinois.edu',
                'overwriteprotocol' => 'https',
                'overwrite.cli.url' => 'https://cloud2.nfs-test.ncsa.illinois.edu'
              );
        cronjob:
          enabled: false
      backups:
        enabled: false
        volume:
          nfs:
            basePath: "/taiga/ncsa/radiant/bbdb/nfs-test/backups/nextcloud2"
            server: "taiga-nfs.ncsa.illinois.edu"
        data:
          enabled: false
stressor2:
  nextcloud:
    url: https://cloud2.nfs-test.ncsa.illinois.edu
  existingSecret: nextcloud-admin
